
Makes is possible to record voice through microphone to a file field.


INSTALLATION
============

1. Download AudioRecorder library from Github:
https://github.com/jwagener/recorder.js

2. Place recorder.js and recorder.swf files into sites/all/libraries/audio-recorder

3. Download & Enable Libraries API module.
http://drupal.org/project/libraries

4. Download & Enable AudioRecorder module.
http://drupal.org/sandbox/zolexiy/1781320


USAGE
=====

After installing the module you will have new file field widget called "Voice recorder".
To start using the Audio recorder add a file filed to your content type and change widget to "Voice recorder".