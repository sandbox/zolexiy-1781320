/**
 * @file
 * Holds GUI implementation for the recorder.
 */

jQuery(document).ready(function ($) {
  var playButtons = jQuery("div.audio-recorder-gui-play");

  jQuery.each(playButtons, function() {
    var playButton = jQuery(this);

    // Setting record button status to assing right CSS class to the play button further.
    playButton.data('disabled', true);

    // Refresh controls.
    Drupal.audioRecorder.buttonsRefresh(playButton);
  })

  // Init widget.
  Drupal.audioRecorder.setup();
});

(function ($) {
  // Adds the recorder object.
  Drupal.audioRecorder = Drupal.audiorecorder || {};

  Drupal.audioRecorder.setup = function () {
    Recorder.initialize({
      swfSrc: Drupal.settings.audioRecorder.swfSrc
    });
  }

  // Turns all button settings into default.
  Drupal.audioRecorder.buttonsRefresh = function (playButton) {
    Drupal.audioRecorder.playButtonDisable(playButton);
  }

  // Disables the record button.
  Drupal.audioRecorder.recordButtonDisable = function (recordButton) {
    recordButton.data('disabled', true).addClass('disabled').removeClass('proceed');
  }

  // Enables the record button.
  Drupal.audioRecorder.recordButtonEnable = function (recordButton) {
    recordButton.data('disabled', false).removeClass('disabled');
  }

  // Disables the play button.
  Drupal.audioRecorder.playButtonDisable = function (playButton) {
    playButton.data('disabled', true).addClass('disabled').removeClass('proceed');
  }

  // Enables the play button.
  Drupal.audioRecorder.playButtonEnable = function () {
    var playButton = jQuery("div.audio-recorder-gui-play");

    playButton.data('disabled', false).removeClass('disabled');
  }

  Drupal.audioRecorder.timecode = function (ms) {
    var hms = {
      h:Math.floor(ms / (60 * 60 * 1000)),
      m:Math.floor((ms / 60000) % 60),
      s:Math.floor((ms / 1000) % 60)
    };
    var tc = [];
    if (hms.h > 0) {
      tc.push(hms.h);
    }
    tc.push((hms.m < 10 && hms.h > 0 ? "0" + hms.m : hms.m));
    tc.push((hms.s < 10 ? "0" + hms.s : hms.s));
    return tc.join(':');
  }

  Drupal.audioRecorder.record = function (recordButton) {
    var playButton = recordButton.siblings('div.audio-recorder-gui-play');

    // Check if we're already plaing something.
    if (recordButton.data('disabled')) {
      return;
    }

    if (recordButton.hasClass('proceed')) {
      // Audio is recording right now.
      Recorder.stop();
      // Enable play button.
      if (playButton.data('disabled')) {
        Drupal.audioRecorder.playButtonEnable(playButton);
      }
      jQuery('a.audio-recorder-upload').removeClass('element-invisible');
    }
    else {
      // Disabling play button for record period.
      Drupal.audioRecorder.playButtonDisable(playButton);
      // Hide upload button.
      jQuery('a.audio-recorder-upload').addClass('element-invisible');
      // Remove status message;
      jQuery('div.audio-recorder-gui div.messages').fadeOut('slow', function () {
        jQuery(this).remove();
      });
      // There is no active recording.
      Recorder.record({
        start:function () {
        },
        progress:function (milliseconds) {
          jQuery("div.audio-recorder-timer").html(Drupal.audioRecorder.timecode(milliseconds));
        }
      });
    }

    // Update button's state.
    recordButton.toggleClass('proceed');
  }

  Drupal.audioRecorder.play = function (playButton) {
    var recordButton = playButton.siblings('div.audio-recorder-gui-record');

    // Check if play is disabled.
    if (playButton.data('disabled')) {
      return;
    }

    // Check button is already active or not.
    if (playButton.hasClass('proceed')) {
      // Someone has already playing the record.
      Recorder.stop();
      // Enable record button back.
      Drupal.audioRecorder.recordButtonEnable(recordButton);
    }
    else {
      // The button is inactive.
      Recorder.play({
        finished:function () {
          // Enable record button.
          Drupal.audioRecorder.recordButtonEnable(recordButton);
          // Remove "proceed" state after playing is finished.
          jQuery("div.audio-recorder-gui-play").removeClass('proceed');
        },
        progress:function (milliseconds) {
          jQuery("div.audio-recorder-timer").html(Drupal.audioRecorder.timecode(milliseconds));
        }
      });
      Drupal.audioRecorder.recordButtonDisable(recordButton);
    }

    // Update button's state.
    playButton.toggleClass('proceed');
  }

  Drupal.audioRecorder.upload = function (fileId) {
    var langcode = Drupal.settings.audioRecorder.langcode,
      delta = Drupal.settings.audioRecorder.fieldDelta,
      fieldName = fileId.split('[').shift(),
      nodeType = Drupal.settings.audioRecorder.nodeType,
      successNotice = jQuery('div.audio-recorder-gui .messages'),
      path = '/audio-recorder/record/' + fieldName + '/' + nodeType,
      uploadButton = jQuery(this);

    // Updating button's state.
    uploadButton.addClass('proceed');

    // Attaching throbber.
    jQuery('div.audio-recorder-container').parents('.form-managed-file').append('<div class="throbber"></div>');

    // Remove notice if one is there.
    if (successNotice.length == 1) {
      successNotice.fadeOut('slow', function() {
        successNotice.remove();
      });
    }

    Recorder.upload({
      url:path,
      audioParam:"",
      params:{},
      success:function (fid) {
        if (fid == null || fid == "") {
          jQuery("#edit-field-" + field_id + "-wrapper").append('<div class="messages error file-upload-js-error">' + Drupal.t('Failed to submit the voice recording!') + '</div>');
        }
        else {
          if (jQuery('div.audio-recorder-gui .messages').length == 0) {
            jQuery('div.audio-recorder-gui').prepend(jQuery('<div class="messages status">' + Drupal.t('Record uploaded.') + '</div>').fadeIn('slow'));
          }
          jQuery('input[name="' + file_id + '"]').val(fid);
        }
        jQuery('.audio-recorder-container').siblings('div.throbber').remove();
      }
    });

  }

  // Attaches click events to the recorder controls.
  Drupal.behaviors.audioRecorder = {
    attach:function () {

      // Record button.
      $('div.audio-recorder-gui-record').click(function () {
        var recordButton = jQuery(this);
        Drupal.audioRecorder.record(recordButton);
      });

      // Play button.
      $('div.audio-recorder-gui-play').click(function () {
        var playButton = jQuery(this);
        Drupal.audioRecorder.play(playButton);
      });

      // Upload button.
      $('a.audio-recorder-upload').click(function () {
        var inputs = jQuery(this).siblings('input'),
          fileId = null;

        jQuery.each(inputs, function() {
          var name = jQuery(this).attr('name');
          if (name.indexOf('[fid]') > 0) {
            fileId = name;
            return false;
          }
        })

        Drupal.audioRecorder.upload(fileId);
        return false;
      });

    }
  }

})(jQuery);
