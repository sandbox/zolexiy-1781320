<?php

/**
 * @file
 * Contains menu callbacks for the module.
 */

/**
 * Recorder callback. This is what happens when 'upload' is pressed.
 */
function _audio_recorder_record($field_name = NULL, $node_type = NULL) {
  if (empty($field_name) || empty($node_type)) {
    $fid = NULL;
  }
  else {
    // @TODO Expose recorder to taxonomy fields.
    $instance = field_info_instance('node', $field_name, $node_type);
    $upload_dir_stream = file_default_scheme() . '://' . $instance['settings']['file_directory'];

    // @TODO Need to care about user entered filename.
    $new_name = time() . '.wav';
    $_FILES['files'] = $_FILES['track'];
    $_FILES['files']['name'] = $new_name;

    foreach ($_FILES['files'] as $key => $value) {
      if ($key == 'tmp_name') {
        $_FILES['files'][$key] = array(0 => $value['asset_data']);
      }
      elseif ($key != 'error') {
        $_FILES['files'][$key] = array(0 => $value);
      }
    }
    $_FILES['files']['error'][0] = UPLOAD_ERR_OK;
    $validators['file_validate_extensions'] = array('wav');
    if (!$file = file_save_upload(0, $validators, $upload_dir_stream)) {
      watchdog('audio_recorder', "ERROR - file_save_upload failed");
      $fid = NULL;
    }
    else {
      $fid = $file->fid;
    }
  }

  print $fid;
}
