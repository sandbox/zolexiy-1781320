<?php

/**
 * @file
 * Theme implementations for Audio Recorder module.
 */

/**
 * Returns audio recorder HTML carcass.
 */
function theme_audio_recorder_carcass($vars) {
  // @TODO Handle to uniqueness for multi-fields.
  $field_delta = $vars['field_delta'];
  $field_name  = $vars['field_name'];

  // Preparing ids list to use in HTML structure.
  $classes = array(
    'audio-recorder-container',
    'audio-recorder-gui',
    'audio-recorder-gui-record',
    'audio-recorder-gui-record-icon',
    'audio-recorder-gui-play',
    'audio-recorder-gui-play-icon',
    'audio-recorder-flash',
  );

  $safe_classes = array();
  foreach ($classes as $delta => $class) {
    // Convert all dashes to underscores.
    $class_name = str_replace('-', '_', $class);
    $safe_classes[$class_name] = drupal_html_class($class);
  }

  extract($safe_classes);

  $widget  = '<div class="' . $audio_recorder_container . '">';

  // Controls.
  $widget .= '<div class="' . $audio_recorder_gui . '">';

  // Record button.
  $widget .= '<div class="' . $audio_recorder_gui_record . '">';
  $widget .= '<div class="' . $audio_recorder_gui_record_icon . '"></div>';
  $widget .= '</div>';

  // Play button.
  $widget .= '<div class="' . $audio_recorder_gui_play . '">';
  $widget .= '<div class="' . $audio_recorder_gui_play_icon . '"></div>';
  $widget .= '</div>';

  // Recorder's flash object.
  $widget .= '<div class="' . $audio_recorder_flash . '"></div>';

  $widget .= '</div></div>';

  return $widget;
}

/**
 * Returns HTML for control buttons.
 */
function theme_audio_recorder_buttons($vars) {
  // @TODO Handle to uniqueness for multi-fields.
  $field_delta = $vars['field_delta'];
  $field_name  = $vars['field_name'];

  $upload = '<a href="#" rel="nofollow" class="audio-recorder-upload button element-invisible">' . t('Upload') . '</a>';

  return $upload;
}
